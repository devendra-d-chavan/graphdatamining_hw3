Build instructions
------------------

 1. Update `build.properties` with the absolute path to the hadoop installation
 2. Run `<path to ant installation>/bin/ant -buildfile build.xml`
 3. The `jar` will be available in `dist` directory

Running the Hadoop job
----------------------
The jar generated above can be run in hadoop using the following command:

    <hadoop executable path> jar <path to the jar file> edu.ncsu.csc.graphdatamining.WordCount <HDFS input directory> <HDFS output file>

Complete list of steps to execute
-----------------------

Assumes that hadoop is installed and configured.

 0. Running on VCL
  0.1. Generate the ssh key `> ssh-keygen -t rsa -P ""`
  0.2. Add the key to the list of authorized keys `> cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys`
  0.3. Change `localhost` to the public IP address in `conf/masters` and `conf/slaves` (single node setup)_
 1. Start the hadoop cluster `> start-all.sh`
 2. Copy the files to the hadoop cluster dfs `> hadoop dfs -copyFromLocal /stanford /graphs`
 3. Confirm that the files have been copied `> hadoop dfs -ls /graphs`
 4. Run the MR job `> hadoop jar degreecount.jar edu.ncsu.csc.graphdatamining.degreecount.Driver /stanford-graphs/amazon.graph /amazon_degree_count`
 5. Copy the output from dfs `> hadoop dfs -getmerge /amazon_degree_count .`
 6. Delete the output from dfs (for subsequent runs) `> hadoop dfs -rmr /amazon_degree_count`
 7. Stop the cluster `> stop-all.sh`

Command line arguments for each task (see Input limitation section)
---------------------

 1. Degree count > hadoop jar degreecount.jar edu.ncsu.csc.graphdatamining.degreecount.Driver /stanford-graphs/amazon.graph /amazon_degree_count
 2. Degree distribution > hadoop jar degreefreq.jar edu.ncsu.csc.graphdatamining.degreefreq.Driver /stanford-graphs/amazon.graph /amazon_degree_freq_dist

Input limitation 
----------------------
The map reduce logic assumes that the input graph does not contain the header
(i.e. the edges and vertices count). This line has to be manually removed from 
the input graph for **accurate** results.
