package edu.ncsu.csc.graphdatamining.degreecount;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Map extends
		Mapper<LongWritable, Text, LongWritable, IntWritable> {
	private final static IntWritable one = new IntWritable(1);

	public static Logger log = Logger.getLogger(Map.class);

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		log.setLevel(Level.WARN);
	}

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String line = value.toString();
		String[] nodes = line.split(" ");
		context.write(new LongWritable(Long.parseLong(nodes[0])), one);
		context.write(new LongWritable(Long.parseLong(nodes[1])), one);
	}
}
