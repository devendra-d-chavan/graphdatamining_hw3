/**
 * 
 */
package edu.ncsu.csc.graphdatamining.degreefreq;

import org.apache.hadoop.fs.FileSystem;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import edu.ncsu.csc.graphdatamining.degreecount.Map;

public class Driver {
	
	public static void main(String[] args) throws Exception {
		// TODO: First line of the input file (header) is not being handled correctly
		Configuration conf = new Configuration();
		Path inputPath = new Path(args[0]);
		Path outputPath = new Path(args[1]);	
		
		// Create a temporary directory to store the intermediate results
		Path tempPath = new Path("temp");
		FileSystem fs = FileSystem.get(tempPath.toUri(), conf);
		fs.delete(tempPath, true);
		   
		runFirstJob(inputPath, tempPath, new Configuration(conf));
		runSecondJob(tempPath, outputPath, new Configuration(conf));
		
		// Cleanup
		fs.delete(tempPath, true);
	}
	
	protected static void runFirstJob(Path input, Path output, Configuration conf) throws Exception
	{
		Job job = new Job(conf, "degreecount");
		job.setJarByClass(edu.ncsu.csc.graphdatamining.degreefreq.Driver.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(IntWritable.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, output);

		job.waitForCompletion(true);		
	}
	
	protected static void runSecondJob(Path input, Path output, Configuration conf) throws Exception
	{
		Job job = new Job(conf, "degreefreq");
		job.setJarByClass(edu.ncsu.csc.graphdatamining.degreefreq.Driver.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(IntWritable.class);

		job.setMapperClass(DegreeFreqMap.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, output);

		job.waitForCompletion(true);		
	}
}