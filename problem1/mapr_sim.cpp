/*
 * mapr_sim.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: xczou, ddchavan
 */

#include <iostream>
#include <vector>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>

using namespace std;

typedef struct{
	int v1;
	int v2;
}edge_t;

/*
 * reducer takes input from each mapper, aggregating hashTable by its key
 */
int reducer(vector<map<int,int> > &intermediate_results, 
            map<int,int> &final_results){

	map<int, int>::iterator found ;
    // Introduce segmentation fault SIGSEGV by accessing beyond the array size
	// for (int i = 0; i <= intermediate_results.size(); ++i) {
	for (int i = 0; i < intermediate_results.size(); ++i) {
		map<int, int> pair = intermediate_results[i];
		for(map<int,int>::iterator itr = pair.begin(); itr != pair.end(); ++itr){
			found = final_results.find(itr->first);
            
            // Key already exist in final results
			if (found != final_results.end()){				
                found->second += itr->second;
			}else {
				final_results[itr->first] = itr->second;
			}
		}
    }

}

/*
 * a list of edges, mapper outputs the key,value pair, key is vertex and value 
 * is vertex degree
 * Note that map structure already sorts the key in non-decreasing order
 */
int mapper(vector<edge_t> &edge_list, map<int, int > &intermediate_results){

	map<int, int>::iterator found ;
	int v;
	for(vector<edge_t>::iterator it = edge_list.begin(); it != edge_list.end(); ++it){
		v = it->v1;
		found = intermediate_results.find(v);
		if (found != intermediate_results.end()){
			found->second++; // increase value by 1
		}else {
			intermediate_results.insert(pair<int,int>(v, 1));
		}

		v = it->v2;
		found = intermediate_results.find(v);
		if (found != intermediate_results.end()){
			found->second++; // increase value by 1
		}else {
			intermediate_results.insert(pair<int,int>(v, 1));
		}
	}
}


int main (int argc, char *argv []){

	FILE *graph_desc_file = 0;
	graph_desc_file = fopen (argv [1], "r");
	assert (graph_desc_file != 0);

	int partition = 2;
	int num_vetices, num_edges;
	fscanf (graph_desc_file, "%d", &num_vetices);
	fscanf (graph_desc_file, "%d\n", &num_edges);

    // Set the denominator of the division operation as zero to produce a 
    // SIGFPE fault
    //partition = 0;
	int partition_size = num_edges / partition ; // edges is divided into 2 parts
	if ( num_edges % partition)
		partition++;   // add one more partition if it is not divisible
	vector<edge_t> edges[partition] ;


	int e = 0, p = 0;

	while (e < num_edges){
		edge_t one_edge;
		fscanf (graph_desc_file, "%d\t%d\n", &(one_edge.v1), &(one_edge.v2));
		p = e/partition_size; // which partition
		edges[p].push_back(one_edge);
		e++;
	}

	vector<map<int,int> > intermediates(partition);
	for (int i = 0; i < partition; ++i) {
		mapper(edges[i], intermediates[i]);
	}

	map<int,int> results;
    // Delete a memory location that was not dynamically allocated to generate
    // a SIGABRT fault
    //map<int,int>* results_ptr = &results;
    //delete results_ptr;

	reducer(intermediates, results);
	for(map<int,int>::iterator itr = results.begin(); itr != results.end(); ++itr){
		cout << itr->first << "\t" << itr->second << endl;
	}
}
