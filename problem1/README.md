Build instructions
-------------------

The code can be build using the `make` command 

    > make

Running the executable
------------------

The output is printed to the console by default. Output redirection allows 
saving the output to a file.

    > ./run_it <path to the graph file>

For example, 

    > ./run_it amazon.graph > amazon_cpp_degree_count

The output is in a tab delimited format for each node,

    <node id>   <degree>
