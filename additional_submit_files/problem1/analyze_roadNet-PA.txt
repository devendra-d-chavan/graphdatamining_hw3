Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 63.44      0.57     0.57  2738116     0.00     0.00  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::find(int const&)
 15.58      0.71     0.14   713508     0.00     0.00  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::lower_bound(int const&)
 14.47      0.84     0.13   715587     0.00     0.00  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::pair<int const, int> const&)
  3.34      0.87     0.03        8     3.76     3.76  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*)
  3.34      0.90     0.03        1    30.05    30.05  load_graph(_IO_FILE*, std::vector<edge_t, std::allocator<edge_t> >*, int, int)
  0.00      0.90     0.00  1429094     0.00     0.00  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_insert(std::_Rb_tree_node_base*, std::_Rb_tree_node_base*, std::pair<int const, int> const&)
  0.00      0.90     0.00   713508     0.00     0.00  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::_Rb_tree_iterator<std::pair<int const, int> >, std::pair<int const, int> const&)
  0.00      0.90     0.00       41     0.00     0.00  std::vector<edge_t, std::allocator<edge_t> >::_M_insert_aux(__gnu_cxx::__normal_iterator<edge_t*, std::vector<edge_t, std::allocator<edge_t> > >, edge_t const&)
  0.00      0.90     0.00        3     0.00   183.99  mapper(std::vector<edge_t, std::allocator<edge_t> >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&)
  0.00      0.90     0.00        3     0.00     0.00  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_copy(std::_Rb_tree_node<std::pair<int const, int> > const*, std::_Rb_tree_node<std::pair<int const, int> >*)
  0.00      0.90     0.00        1     0.00     0.00  global constructors keyed to _Z6dclockv
  0.00      0.90     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      0.90     0.00        1     0.00   300.73  reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&)
  0.00      0.90     0.00        1     0.00     0.00  void std::__uninitialized_fill_n_a<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >)
  0.00      0.90     0.00        1     0.00     0.00  void std::__uninitialized_fill_n_aux<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, __false_type)

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.
 
 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this 
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 1.11% of 0.90 seconds

index % time    self  children    called     name
                                                 <spontaneous>
[1]    100.0    0.00    0.90                 main [1]
                0.00    0.55       3/3           mapper(std::vector<edge_t, std::allocator<edge_t> >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [3]
                0.00    0.30       1/1           reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
                0.03    0.00       1/1           load_graph(_IO_FILE*, std::vector<edge_t, std::allocator<edge_t> >*, int, int) [8]
                0.02    0.00       5/8           std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*) [7]
                0.00    0.00       1/1           void std::__uninitialized_fill_n_a<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >) [21]
-----------------------------------------------
                0.15    0.00  715586/2738116     reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
                0.42    0.00 2022530/2738116     mapper(std::vector<edge_t, std::allocator<edge_t> >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [3]
[2]     63.3    0.57    0.00 2738116         std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::find(int const&) [2]
-----------------------------------------------
                0.00    0.55       3/3           main [1]
[3]     61.2    0.00    0.55       3         mapper(std::vector<edge_t, std::allocator<edge_t> >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [3]
                0.42    0.00 2022530/2738116     std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::find(int const&) [2]
                0.13    0.00  715586/715587      std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::pair<int const, int> const&) [6]
-----------------------------------------------
                0.00    0.30       1/1           main [1]
[4]     33.4    0.00    0.30       1         reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
                0.15    0.00  715586/2738116     std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::find(int const&) [2]
                0.14    0.00  713508/713508      std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::lower_bound(int const&) [5]
                0.01    0.00       3/8           std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*) [7]
                0.00    0.00  713508/713508      std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::_Rb_tree_iterator<std::pair<int const, int> >, std::pair<int const, int> const&) [9]
                0.00    0.00       3/3           std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_copy(std::_Rb_tree_node<std::pair<int const, int> > const*, std::_Rb_tree_node<std::pair<int const, int> >*) [18]
-----------------------------------------------
                0.14    0.00  713508/713508      reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
[5]     15.6    0.14    0.00  713508         std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::lower_bound(int const&) [5]
-----------------------------------------------
                0.00    0.00       1/715587      std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::_Rb_tree_iterator<std::pair<int const, int> >, std::pair<int const, int> const&) [9]
                0.13    0.00  715586/715587      mapper(std::vector<edge_t, std::allocator<edge_t> >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [3]
[6]     14.4    0.13    0.00  715587         std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::pair<int const, int> const&) [6]
                0.00    0.00  715587/1429094     std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_insert(std::_Rb_tree_node_base*, std::_Rb_tree_node_base*, std::pair<int const, int> const&) [16]
-----------------------------------------------
                             2144680             std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*) [7]
                0.01    0.00       3/8           reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
                0.02    0.00       5/8           main [1]
[7]      3.3    0.03    0.00       8+2144680 std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*) [7]
                             2144680             std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*) [7]
-----------------------------------------------
                0.03    0.00       1/1           main [1]
[8]      3.3    0.03    0.00       1         load_graph(_IO_FILE*, std::vector<edge_t, std::allocator<edge_t> >*, int, int) [8]
                0.00    0.00      41/41          std::vector<edge_t, std::allocator<edge_t> >::_M_insert_aux(__gnu_cxx::__normal_iterator<edge_t*, std::vector<edge_t, std::allocator<edge_t> > >, edge_t const&) [17]
-----------------------------------------------
                0.00    0.00  713508/713508      reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
[9]      0.0    0.00    0.00  713508         std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::_Rb_tree_iterator<std::pair<int const, int> >, std::pair<int const, int> const&) [9]
                0.00    0.00       1/715587      std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::pair<int const, int> const&) [6]
                0.00    0.00  713507/1429094     std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_insert(std::_Rb_tree_node_base*, std::_Rb_tree_node_base*, std::pair<int const, int> const&) [16]
-----------------------------------------------
                0.00    0.00  713507/1429094     std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::_Rb_tree_iterator<std::pair<int const, int> >, std::pair<int const, int> const&) [9]
                0.00    0.00  715587/1429094     std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::pair<int const, int> const&) [6]
[16]     0.0    0.00    0.00 1429094         std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_insert(std::_Rb_tree_node_base*, std::_Rb_tree_node_base*, std::pair<int const, int> const&) [16]
-----------------------------------------------
                0.00    0.00      41/41          load_graph(_IO_FILE*, std::vector<edge_t, std::allocator<edge_t> >*, int, int) [8]
[17]     0.0    0.00    0.00      41         std::vector<edge_t, std::allocator<edge_t> >::_M_insert_aux(__gnu_cxx::__normal_iterator<edge_t*, std::vector<edge_t, std::allocator<edge_t> > >, edge_t const&) [17]
-----------------------------------------------
                              357807             std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_copy(std::_Rb_tree_node<std::pair<int const, int> > const*, std::_Rb_tree_node<std::pair<int const, int> >*) [18]
                0.00    0.00       3/3           reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [4]
[18]     0.0    0.00    0.00       3+357807  std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_copy(std::_Rb_tree_node<std::pair<int const, int> > const*, std::_Rb_tree_node<std::pair<int const, int> >*) [18]
                              357807             std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_copy(std::_Rb_tree_node<std::pair<int const, int> > const*, std::_Rb_tree_node<std::pair<int const, int> >*) [18]
-----------------------------------------------
                0.00    0.00       1/1           __do_global_ctors_aux [24]
[19]     0.0    0.00    0.00       1         global constructors keyed to _Z6dclockv [19]
                0.00    0.00       1/1           __static_initialization_and_destruction_0(int, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           global constructors keyed to _Z6dclockv [19]
[20]     0.0    0.00    0.00       1         __static_initialization_and_destruction_0(int, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         void std::__uninitialized_fill_n_a<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >) [21]
                0.00    0.00       1/1           void std::__uninitialized_fill_n_aux<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, __false_type) [22]
-----------------------------------------------
                0.00    0.00       1/1           void std::__uninitialized_fill_n_a<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >) [21]
[22]     0.0    0.00    0.00       1         void std::__uninitialized_fill_n_aux<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, __false_type) [22]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.


Index by function name

  [19] global constructors keyed to _Z6dclockv (mapr_sim.cpp) [17] std::vector<edge_t, std::allocator<edge_t> >::_M_insert_aux(__gnu_cxx::__normal_iterator<edge_t*, std::vector<edge_t, std::allocator<edge_t> > >, edge_t const&) [18] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_copy(std::_Rb_tree_node<std::pair<int const, int> > const*, std::_Rb_tree_node<std::pair<int const, int> >*)
   [8] load_graph(_IO_FILE*, std::vector<edge_t, std::allocator<edge_t> >*, int, int) [5] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::lower_bound(int const&) [7] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_erase(std::_Rb_tree_node<std::pair<int const, int> >*)
  [20] __static_initialization_and_destruction_0(int, int) (mapr_sim.cpp) [6] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::pair<int const, int> const&) [16] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::_M_insert(std::_Rb_tree_node_base*, std::_Rb_tree_node_base*, std::pair<int const, int> const&)
   [3] mapper(std::vector<edge_t, std::allocator<edge_t> >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [9] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::insert_unique(std::_Rb_tree_iterator<std::pair<int const, int> >, std::pair<int const, int> const&) [21] void std::__uninitialized_fill_n_a<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >)
   [4] reducer(std::vector<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >, std::allocator<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > > >&, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >&) [2] std::_Rb_tree<int, std::pair<int const, int>, std::_Select1st<std::pair<int const, int> >, std::less<int>, std::allocator<std::pair<int const, int> > >::find(int const&) [22] void std::__uninitialized_fill_n_aux<std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > >(std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > >*, unsigned long, std::map<int, int, std::less<int>, std::allocator<std::pair<int const, int> > > const&, __false_type)
