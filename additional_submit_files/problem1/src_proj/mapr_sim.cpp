/*
 * mapr_sim.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: xczou
 */

#include <iostream>
#include <vector>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>

using namespace std;

typedef struct {
	int v1;
	int v2;
} edge_t;

//turn on/off the timing/output 
#define TIMING 0

#define OUTPUT 0

double dclock(void) {
	struct timeval tv;
	gettimeofday(&tv, 0);

	return (double) tv.tv_sec + (double) tv.tv_usec * 1e-6;
}

/*
 * reducer takes input from each mapper, aggregating hashTable by its key
 */
int reducer(vector<map<int, int> > &intermediate_results,
		map<int, int> &final_results) {

	map<int, int>::iterator found;
	for (int i = 0; i < intermediate_results.size(); ++i) {
		map<int, int> pair = intermediate_results[i];
		for (map<int, int>::iterator itr = pair.begin(); itr != pair.end(); ++itr) {
			found = final_results.find(itr->first);
			if (found != final_results.end()) {// key already exist in final results
				found->second += itr->second;
			} else {
				final_results[itr->first] = itr->second;
			}
		}
	}

}

/*
 * a list of edges, mapper outputs the key,value pair, key is vertex and value is vertex degree
 * Note that map structure already sorts the key in non-decreasing order
 */
int mapper(vector<edge_t> &edge_list, map<int, int> &intermediate_results) {

	map<int, int>::iterator found;
	int v;
	for (vector<edge_t>::iterator it = edge_list.begin(); it != edge_list.end(); ++it) {
		v = it->v1;
		found = intermediate_results.find(v);
		if (found != intermediate_results.end()) {
			found->second++; // increase value by 1
		} else {
			intermediate_results.insert(pair<int, int> (v, 1));
		}

		v = it->v2;
		found = intermediate_results.find(v);
		if (found != intermediate_results.end()) {
			found->second++; // increase value by 1
		} else {
			intermediate_results.insert(pair<int, int> (v, 1));
		}
	}
}
/*
 * This purpose of function is to evaluate the I/O time of load the graph data
 */
int load_graph(FILE *graph_desc_file, vector<edge_t> *edges, int num_edges, int partition_size) {

	int e = 0, p = 0;
	int r = 0;
	while (e < num_edges) {
		edge_t one_edge;
		r = fscanf(graph_desc_file, "%d\t%d\n", &(one_edge.v1), &(one_edge.v2));
		p = e / partition_size; // which partition
		edges[p].push_back(one_edge);
		e++;
	}
}

int main(int argc, char *argv[]) {

	if (argc < 2) {
		cout << "input graph data file, please " << endl;
		return 1;
	}
#if TIMING
	double s  =dclock();
	double io_s = s;
	double io, mapper_t,  mapper_s , reducer_s ;
#endif

	int partition = 1;

	FILE *graph_desc_file = 0;
	graph_desc_file = fopen(argv[1], "r");
	assert (graph_desc_file != 0);

	int num_vetices, num_edges, t;
	t = fscanf(graph_desc_file, "%d", &num_vetices);
	t = fscanf(graph_desc_file, "%d\n", &num_edges);

	int partition_size = num_edges / partition; // edges is divided into 2 parts
	if (num_edges % partition)
		partition += 1; // add one more partition if it is not divisible


	vector<edge_t> edges[partition];

	load_graph(graph_desc_file, edges, num_edges, partition_size);


#if TIMING
	io = dclock() - io_s;
	mapper_s = dclock();
#endif


	vector<map<int, int> > intermediates(partition);
	for (int i = 0; i < partition; ++i) {
		mapper(edges[i], intermediates[i]);
	}




#if TIMING
	mapper_t = dclock() - mapper_s;
	reducer_s = dclock();
#endif


	map<int, int> results;
	reducer(intermediates, results);

#if TIMING
	printf("Timing: total[%f], I/O[%f], Mapper[%f], Reducer[%f]\n",
			dclock() - s, io, mapper_t, dclock() - reducer_s );
#endif

#if OUTPUT
	for (map<int, int>::iterator itr = results.begin(); itr != results.end(); ++itr) {
		cout << itr->first << ":" << itr->second << endl;
	}
#endif


	return 1;

}
